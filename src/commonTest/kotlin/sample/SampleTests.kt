package sample

import io.mockk.every
import io.mockk.mockkConstructor
import io.mockk.mockkObject
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SampleTests {
    @Test
    fun testMe() {
        assertTrue(Sample().checkMe() > 0)
    }

    @Test
    fun testWithAnotherEntitysUnimplementedMethod() {
        val target = TestTarget()
        mockkConstructor(AnotherEntity::class)
        every { anyConstructed<AnotherEntity>().toTestTarget() } returns target
        assertEquals(target, target.test())
    }

    @Test
    fun testWithUnimplementedMethod() {
        val testTarget = TestTarget()
        mockkObject(testTarget)
        every { testTarget.unimplementedFunction() } returns AnotherEntity(2)
        assertEquals(AnotherEntity(2), testTarget.unimplementedFunctionCaller())
    }
}
